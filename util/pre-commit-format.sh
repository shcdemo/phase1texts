#/bin/sh


staged_files=`git diff --cached --name-status      |
                egrep -i '^(A|M).*\.(xml)$'        | # Only process certain files
                sed -e 's/^[AM][[:space:]]*//'     | # Remove leading git info
                sort                               | # Remove duplicates
                uniq`

for FILE in $staged_files ; do

# Sort attributes by name.
    tmp="$(mktemp)"

    xalan -i 1 -v -o "$tmp" "$FILE" - << STYLESHEET || { echo 'xalan: attribute sort failed for:' "$FILE" >&2; exit 1; }
<?xml version="1.0"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xalan="http://xml.apache.org/xslt">
  <xsl:output method="xml" encoding="UTF-8" indent="yes" xalan:indent-amount="1"/>
  <xsl:strip-space elements="*"/>
  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates select="@*">
        <xsl:sort select="name()"/>
      </xsl:apply-templates>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="@*|comment()|processing-instruction()">
    <xsl:copy />
  </xsl:template>
</xsl:stylesheet>
STYLESHEET

    mv "$tmp" "$FILE"

# Format with single space as the indent.

    tmp="$(mktemp)"
    XMLLINT_INDENT=$' ' xmllint --huge --encode UTF-8 --format "$FILE" --output "$tmp" 2>/dev/null || {
        echo 'fatal: `xmllint --format` failed for: ' "$FILE" >&2
        exit 1
    }
    mv "$tmp" "$FILE"
    git add "$FILE"

done
